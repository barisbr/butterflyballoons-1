const mongoose = require('mongoose');

let schema = new mongoose.Schema({
  Code: {
    type: String,
    required: true,
    index: true,
    unique: true,
  },
  IsUsed: {
    type: Boolean,
    required: false,
    default: false,
    index: true,
  },
  DiscountAmount: {
    type: Number,
    default: null,
  },
  DiscountCurrency: {
    type: String,
    default: null,
  },
  Currencies: {
    type: Object,
    default: null,
  }
})

const model = mongoose.model('coupons', schema, 'coupons')

exports.Coupon = model
